﻿using AutoMapper;
using FirstMVCApp.DAL;
using FirstMVCApp.DAL.Entities;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FirstMVCApp.Models.Services
{
    public class ProductService : IProductService
    {
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;

        public ProductService(IUnitOfWorkFactory unitOfWorkFactory)
        {

            if (unitOfWorkFactory == null)
                throw new ArgumentNullException(nameof(unitOfWorkFactory));
            _unitOfWorkFactory = unitOfWorkFactory;
        }

       

        public List<ProductModel> SearchProduct(ProductFilterModel model)
        {
            using (var uow = _unitOfWorkFactory.Create())
            {

                IEnumerable<Product> products = uow.Products.GetAllWithCategoryAndBrands();
                products = products.ByPriceFrom(model.PriceFrom)
                    .ByPriceTo(model.PriceTo)
                    .ByName(model.Name)
                    .ByCategoryId(model.CategoryId, uow)
                    .ByBrandId(model.BrandId, uow);


                List<ProductModel> productModels = Mapper.Map<List<ProductModel>>(products);
                return productModels;
            }
        }

        public ProductCreateModel GetProductCreateModel()
        {
            using (var uow = _unitOfWorkFactory.Create())
            {
                var Categories = uow.Categories.GetAll().ToList();
                var Brands = uow.Brands.GetAll().ToList();
                return new ProductCreateModel()
                {
                    CategoriesList = new SelectList(Categories, nameof(Category.Id), nameof(Category.Name)),
                    BrandsList = new SelectList(Brands, nameof(Brand.Id), nameof(Brand.Name))
                };
            }
        }

        public void CreateProduct(ProductCreateModel model)
        {
            using (var uow = _unitOfWorkFactory.Create())
            {
                Product product = Mapper.Map<Product>(model);

                uow.Products.Create(product);
            }
        }
    }
}

