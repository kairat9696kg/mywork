﻿using System.Collections.Generic;

namespace FirstMVCApp.Models.Services
{
    public interface IBrandService
    {
        public IEnumerable<BrandModel> GetAllBrands();
        void CreateBrand(BrandCreateModel model);
    }
}
